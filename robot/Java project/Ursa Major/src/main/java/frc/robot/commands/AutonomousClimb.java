/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.ArmSetPointSubsystem;

public class AutonomousClimb extends Command {
        
    double leftLegValue = 0;
    double rightLegValue = 0;

    double frontMotorOutput = 0;
    double backMotorOutput = 0;

    boolean left_stopped = false;
    boolean right_stopped = false;

    public static final double ANGLE_ARM_STOP_DIFF = 475;

    public AutonomousClimb() {
        requires(Robot.armSetPointSubsystem);
        requires(Robot.climbSubsystem);
    }
    
    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
        this.left_stopped = false;
        this.right_stopped = false;
    }

    private void RefreshLegsDifference(double roll) {
        // Roll is between -1 and 1
        this.leftLegValue = RobotMap.Roll_P * roll;
        this.rightLegValue = -RobotMap.Roll_P * roll;
    }

    private void RefreshPitchOutputs(double pitch) {
        this.frontMotorOutput = RobotMap.Pitch_P * pitch;
        this.backMotorOutput = -RobotMap.Pitch_P * pitch;
    }

    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
        // from gyro
        double roll = Robot.climbSubsystem.getRoll()/90;
        double pitch = Robot.climbSubsystem.getPitch()/90;

        // Read Roll & pitch values

        // Get motor outputs
        RefreshLegsDifference(pitch);
        RefreshPitchOutputs(roll);

        double leftLegMotorOutput = 
            0.1 * this.backMotorOutput + 
            0.1 * this.leftLegValue + 
            0.8;
        double rightLegMotorOutput = 
            0.1 * this.backMotorOutput + 
            0.1 * this.rightLegValue + 
            0.8;

        double armMotorOutput = 
            0.7 * this.frontMotorOutput +
            0.3;

        // Pass to motors
        Robot.armPID.climbControl(-armMotorOutput);
        if (!left_stopped) {
            Robot.climbSubsystem.climbControlLeftLeg(-leftLegMotorOutput);
        }
        if (!right_stopped) {
            Robot.climbSubsystem.climbControlRightLeg(-rightLegMotorOutput);
        }
        
        SmartDashboard.putNumber("ArmMotorOutput", armMotorOutput);
        SmartDashboard.putNumber("LeftLegMotorOutput", leftLegMotorOutput);
        SmartDashboard.putNumber("RightLegMotorOutput", rightLegMotorOutput);
    }

    private boolean reachedArmAngleStop() {
        return Robot.armPID.getPotentiometerValue() - Robot.armPID.getInitialPotentiometerValue() > ANGLE_ARM_STOP_DIFF;
    }

    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished() {
        if (Robot.climbSubsystem.downLeftSwitch() || Robot.climbSubsystem.downRighSwitch()) {
            if (Robot.climbSubsystem.downLeftSwitch()) {
                Robot.climbSubsystem.stopLeftMotor();
                left_stopped = true;
            } else {
                Robot.climbSubsystem.stopRightMotor();
                right_stopped = true;
            }
        }

        return Robot.climbSubsystem.IsPressedDownSwitch() || reachedArmAngleStop();
    }
    
    // Called once after isFinished returns true
    @Override
    protected void end() {
        Robot.climbSubsystem.stopAllMotors();
        left_stopped = true;
        right_stopped = true;
    }
    
    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
        end();
    }
    
}
