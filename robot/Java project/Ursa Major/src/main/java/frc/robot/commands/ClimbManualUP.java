/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class ClimbManualUP extends Command {
    public ClimbManualUP() {
        requires(Robot.climbSubsystem);
    }
    
    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
        Robot.climbSubsystem.ManualUP();
    }
    
    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
        if(Robot.climbSubsystem.m_upLeftSwitch.get()){
            Robot.climbSubsystem.m_leftClimbMotor.stopMotor();
        }
        if(Robot.climbSubsystem.m_upRightSwitch.get()){
            Robot.climbSubsystem.m_rightClimbMotor.stopMotor();
        }
    }
    
    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished() {
        if(Robot.climbSubsystem.IsPressedUPSwitch()){
            return true;
        }
        else{
            return false;
        }
    }
    
    // Called once after isFinished returns true
    @Override
    protected void end() {
        // Robot.climbSubsystem.m_climbMotor.stopMotor();
        Robot.climbSubsystem.m_leftClimbMotor.stopMotor();
        Robot.climbSubsystem.m_rightClimbMotor.stopMotor();
    }
    
    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
        end();
    }
}
