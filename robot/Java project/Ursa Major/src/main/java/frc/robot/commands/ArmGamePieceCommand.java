/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.Robot.ArmState;

public class ArmGamePieceCommand extends Command {
    
    private double setPointValue;
    private int m_onTargetCount = 0;
    private static final int MAX_COUNT = 70; // TODO: check this value
    
    public ArmGamePieceCommand(double setPointValue) {
        this.setPointValue = setPointValue;
        requires(Robot.armSetPointSubsystem);
    }
    
    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
        Robot.armPID.setPoint(this.setPointValue);
    }
    
    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
    }
    
    // Make this return true when this Command no longer needs to run execute()
    @Override
    protected boolean isFinished() {
        if (Robot.armPID.isOnTarget()) {
            this.m_onTargetCount++;
        } else {
            this.m_onTargetCount = 0;
        }
        
        return this.m_onTargetCount >= MAX_COUNT || Robot.m_armState == ArmState.MANUAL;
    }
    
    // Called once after isFinished returns true
    @Override
    protected void end() {
    }
    
    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
    }
    
}
