/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
//import edu.wpi.first.wpilibj.interfaces.Potentiometer;
//import edu.wpi.first.wpilibj.AnalogInput;
//import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DigitalInput;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;


/**
* Add your docs here.
*/
public class ClimbSubsystem extends Subsystem {    

    public AHRS NavX;
    public WPI_TalonSRX m_leftClimbMotor;
    public WPI_TalonSRX m_rightClimbMotor;
    public SpeedControllerGroup m_climbMotor;
    public DigitalInput m_downLeftSwitch;
    public DigitalInput m_downRightSwitch;
    public DigitalInput m_upLeftSwitch;
    public DigitalInput m_upRightSwitch;

    //Potentiometer
    //public Potentiometer Potentiometer;

    public static final double LEFT_MOTOR_DOWN = -1;
    public static final double RIGHT_MOTOR_DOWN = -1;
    public static final double LEFT_MOTOR_UP = 0.8;
    public static final double RIGHT_MOTOR_UP = 0.8;
    
    public ClimbSubsystem() {
        this.m_leftClimbMotor = new WPI_TalonSRX(RobotMap.LeftClimbMotor);
        this.m_rightClimbMotor = new WPI_TalonSRX(RobotMap.RightClimbMotor);
        //this.m_climbMotor = new SpeedControllerGroup(this.m_leftClimbMotor, this.m_rightClimbMotor);
        this.m_rightClimbMotor.setInverted(true);

        this.m_downLeftSwitch = new DigitalInput(RobotMap.downLeftClimbSwitch); 
        this.m_downRightSwitch = new DigitalInput(RobotMap.downRightClimbSwitch);
        this.m_upLeftSwitch = new DigitalInput(RobotMap.upLeftClimbSwitch); 
        this.m_upRightSwitch = new DigitalInput(RobotMap.upRightClimbSwitch);

        this.NavX = new AHRS(SPI.Port.kMXP);
        

        //Potentiometer = new AnalogPotentiometer(2, 3600, 0);
        // Might be only manual control
    }
/* enable in case that needed
    public void CurrentLimiting(){
        int peakAmps = 80;
        int peakDuration = 200;
        int continuousCurrentLimit = 40;

        m_leftClimbMotor.configPeakCurrentLimit(peakAmps);
        m_leftClimbMotor.configPeakCurrentDuration(peakDuration);
        m_leftClimbMotor.configContinuousCurrentLimit(continuousCurrentLimit);

        m_rightClimbMotor.configPeakCurrentLimit(peakAmps);
        m_rightClimbMotor.configPeakCurrentDuration(peakDuration);
        m_rightClimbMotor.configContinuousCurrentLimit(continuousCurrentLimit);


    }
    */


    public boolean IsPressedDownSwitch(){
        if(this.m_downLeftSwitch.get() && this.m_downRightSwitch.get()) {
            return true;
        }
        else{
            return false;
        }
    }

    public boolean IsPressedUPSwitch() {
        if(this.m_upLeftSwitch.get() && this.m_upRightSwitch.get()) {
            return true;
        }
        else{
            return false;
        }
    }
    
    public void ManualUP() {
        this.m_leftClimbMotor.set(1);//0.8
        this.m_rightClimbMotor.set(1);//0.8
    }
    
    public void ManualDown() {
        this.m_leftClimbMotor.set(-0.7);
        this.m_rightClimbMotor.set(-0.7);
    }

    @Override
    public void initDefaultCommand() {
    }

    public boolean downLeftSwitch(){
        return this.m_downLeftSwitch.get();
    }

    public boolean downRighSwitch(){
        return this.m_downRightSwitch.get();
    }
    
    public boolean upLeftSwitch(){
        return this.m_upLeftSwitch.get();
    }

    public boolean upRighSwitch(){
        return this.m_upRightSwitch.get();
    }

    

    public void climbControlRightLeg(double value){
        this.m_rightClimbMotor.set(value);
    }

    public void climbControlLeftLeg(double value){
        this.m_leftClimbMotor.set(value);
    }

    public double getPitch() {
        return this.NavX.getPitch();   
    }

    public double getRoll() {
        return this.NavX.getRoll();
    }

    public void reportVariabls() {
        SmartDashboard.putNumber("Pitch", this.getPitch());
        SmartDashboard.putNumber("Roll", this.getRoll());
    }

    public void stopAllMotors() {
        m_leftClimbMotor.set(0);
        m_rightClimbMotor.set(0);
    }

    public void stopLeftMotor() {
        m_leftClimbMotor.set(0);
    }

    public void stopRightMotor() {
        m_rightClimbMotor.set(0);
    }
}
