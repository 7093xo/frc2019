/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.cameraserver.CameraServer;
import frc.robot.RobotMap;

import java.sql.Driver;

// import com.sun.security.ntlm.Server;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoSink;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.commands.DriveSpeedCommand;
import frc.robot.subsystems.ElevatorPIDSubsystem;
import frc.robot.subsystems.CollectorSubsystem;
import frc.robot.subsystems.ElevatorSetPointSubsystem;
import frc.robot.subsystems.InfraredSubsystem;
import frc.robot.subsystems.ArmPIDSubsystem;
import frc.robot.subsystems.ArmSetPointSubsystem;
import frc.robot.subsystems.ClawCargoSubsystem;
import frc.robot.subsystems.ClimbSubsystem;
import frc.robot.subsystems.PivotSetPointSubsystem;
import frc.robot.subsystems.PivotPIDSubsystem;
import frc.robot.subsystems.ClawSetPointSubsystem;
import frc.robot.subsystems.ClawPIDSubsystem;
import frc.robot.commands.ManualUPElevator;
import frc.robot.commands.ManualDownElevator;
import frc.robot.commands.PivotManualUP;
import frc.robot.commands.SetAutonomousClimbWeights;
import frc.robot.commands.SetPIDArmCommand;
import frc.robot.commands.SetPIDElevatorCommand;
import frc.robot.commands.SetPIDPivotCommand;
import frc.robot.commands.SetPIDClawCommand;
import frc.robot.commands.SyncedClimbCG;
import frc.robot.commands.PivotManualDown;
import frc.robot.commands.ElevatorReset;
import frc.robot.commands.Fold;
import frc.robot.commands.FullAutonomousClimb;
import frc.robot.commands.ElevatorGamePieceCommand;
import frc.robot.commands.PivotGamePieceCommand;
import frc.robot.commands.ArmGamePieceCommand;
import frc.robot.commands.ArmManualDown;
import frc.robot.commands.ArmManualUp;
import frc.robot.commands.CGCargoHigh;
import frc.robot.commands.CGCargoLow;
import frc.robot.commands.CGCargoMid;
import frc.robot.commands.CGCargoShip;
import frc.robot.commands.CGHatchHigh;
import frc.robot.commands.CGHatchLow;
import frc.robot.commands.CGHatchMid;
import frc.robot.commands.ClawCloseManual;
import frc.robot.commands.ClawGamePieceCommand;
import frc.robot.commands.ClawOpenManual;
import frc.robot.commands.AutonomousClimb;
import frc.robot.commands.ClimbManualDown;
import frc.robot.commands.ClimbManualUP;
import frc.robot.commands.CloseOnCargoCG;
import frc.robot.commands.EjectCommand;
import frc.robot.commands.CollectCommand;
import frc.robot.commands.CollectingCargoFromField;
import frc.robot.commands.ClawCargoCollect;
import frc.robot.commands.ClawCargoEject;
import edu.wpi.first.wpilibj.Timer;
/**
* The VM is configured to automatically run this class, and to call the
* functions corresponding to each mode, as described in the TimedRobot
* documentation. If you change the name of this class or the package after
* creating this project, you must also update the build.gradle file in the
* project.
*/
public class Robot extends TimedRobot {
    VideoSink serverForCamera;
    UsbCamera DriversCameraBack;
    UsbCamera DriversCameraFront;
    
    public enum ElevatorState {
        MANUAL,
        PID
    }
    public enum ArmState {
        MANUAL,
        PID
    }
    public enum PivotState {
        MANUAL,
        PID
    }
    public enum ClawState {
        MANUAL,
        PID
    }
    public enum Mode{
        COMPETITIOM,
        TEST_1,
        TEST_2
    }
    public static OI m_oi;
    private final Timer m_timer = new Timer();
    //m_Chooser
    SendableChooser<Command> m_chooser = new SendableChooser<>();
    
    /*//Claw Chooser
    Subsystem m_ClawSubsystem;
    SendableChooser<Subsystem> m_ClawChooser = new SendableChooser<>(); // TODO: read later
    */
    
    //Subsystems Declaration
    public static DriveSubsystem RobotDrive;
    public static CollectorSubsystem collectorSusbsytem; 
    public static ElevatorSetPointSubsystem elevatorSetPointSubsystem;
    public static ElevatorPIDSubsystem elevatorPID;
    public static ArmSetPointSubsystem armSetPointSubsystem;
    public static ArmPIDSubsystem armPID;
    public static PivotPIDSubsystem pivotPID;
    public static ClawPIDSubsystem clawPID;
    public static PivotSetPointSubsystem pivotSetPointSubsystem;
    public static ClawSetPointSubsystem clawSetPointSubsystem;
    public static InfraredSubsystem infraredSubsystem;
    public static ClimbSubsystem climbSubsystem;
    public static ClawCargoSubsystem clawCargoSubsystem;


    public static ElevatorState m_elevatorState;
    public static ArmState m_armState;
    public static ClawState m_clawState;
    public static PivotState m_pivotState;
    
    private void parametersInit() {
        SmartDashboard.putNumber(RobotMap.Arm_PID_P_Name, RobotMap.Arm_PID_P);
        SmartDashboard.putNumber(RobotMap.Arm_PID_I_Name, RobotMap.Arm_PID_I);
        SmartDashboard.putNumber(RobotMap.Arm_PID_D_Name, RobotMap.Arm_PID_D);

        SmartDashboard.putNumber(RobotMap.Elevator_PID_P_Name, RobotMap.Elevator_PID_P);
        SmartDashboard.putNumber(RobotMap.Elevator_PID_I_Name, RobotMap.Elevator_PID_I);
        SmartDashboard.putNumber(RobotMap.Elevator_PID_D_Name, RobotMap.Elevator_PID_D);

        SmartDashboard.putNumber(RobotMap.Pivot_PID_P_Name, RobotMap.Pivot_PID_P);
        SmartDashboard.putNumber(RobotMap.Pivot_PID_I_Name, RobotMap.Pivot_PID_I);
        SmartDashboard.putNumber(RobotMap.Pivot_PID_D_Name, RobotMap.Pivot_PID_D);        

        SmartDashboard.putNumber(RobotMap.Claw_PID_P_Name, RobotMap.Claw_PID_P);
        SmartDashboard.putNumber(RobotMap.Claw_PID_I_Name, RobotMap.Claw_PID_I);
        SmartDashboard.putNumber(RobotMap.Claw_PID_D_Name, RobotMap.Claw_PID_D); 
        
        SmartDashboard.putNumber(RobotMap.Roll_P_Name, RobotMap.Roll_P); 
        SmartDashboard.putNumber(RobotMap.Pitch_P_Name, RobotMap.Pitch_P);
        
    }
    
    /**
    * This function is run when the robot is first started up and should be
    * used for any initialization code.
    */
    @Override
    public void robotInit() {
        parametersInit();
        
        m_timer.reset();
        m_timer.start();
        
        //Create an object
        m_oi = new OI();
        
        m_elevatorState = ElevatorState.PID;
        m_armState = ArmState.PID;
        m_clawState = ClawState.PID;
        m_pivotState = PivotState.PID;
        /*m_ClawChooser.addDefault("Claw Hatch", new ClawSubsystem()); // TODO: not new subsystems but rather the existing ones
        m_ClawChooser.addObject("Claw Cargo", new armSetPointSubsystem());
        SmartDashboard.putData("Claw mode", m_ClawChooser);
        */
        try {
            //Drivers Camera Front
            DriversCameraFront = CameraServer.getInstance().startAutomaticCapture(0);
            DriversCameraFront.setFPS(30);
            DriversCameraFront.setExposureAuto();
            DriversCameraFront.setWhiteBalanceAuto();
            DriversCameraFront.setResolution(160, 120);
        
            DriversCameraBack = CameraServer.getInstance().startAutomaticCapture(1);
            DriversCameraBack.setExposureAuto();
            DriversCameraBack.setFPS(30);
            DriversCameraBack.setResolution(160, 120);
            DriversCameraBack.setWhiteBalanceAuto();
            
            serverForCamera = CameraServer.getInstance().getServer();
        }
        catch (Exception e) {
            // TODO: Figure out why this happens on simulation
        }
        
        //Subsystem objects
        RobotDrive = new DriveSubsystem(); //Drive 
        elevatorPID = new ElevatorPIDSubsystem();
        elevatorSetPointSubsystem = new ElevatorSetPointSubsystem(); // Elevator
        
        armPID = new ArmPIDSubsystem();
        armSetPointSubsystem = new ArmSetPointSubsystem(armPID.getInitialPotentiometerValue()); //Arm
        
        pivotPID = new PivotPIDSubsystem();
        pivotSetPointSubsystem = new PivotSetPointSubsystem();
        
        clawPID = new ClawPIDSubsystem();
        clawCargoSubsystem = new ClawCargoSubsystem(); //ClawCargo
        clawSetPointSubsystem = new ClawSetPointSubsystem(clawPID.getInitialPotentiometerValue()); //Claw
        
        infraredSubsystem = new InfraredSubsystem(); // IR
        climbSubsystem = new ClimbSubsystem(); //Climb
        collectorSusbsytem = new CollectorSubsystem(); //Collector
        


        // Note - This must happen after the robot subsystems have been initialized
        RobotMap.refreshConfiguration();
        
        Mode mode = Mode.COMPETITIOM;
        // Mode mode = Mode.TEST_1;
        if(mode == Mode.COMPETITIOM) {
            /////////////////////////////////////// ! Drive joystick !////////////////////////////////////////////////
            OI.buttonDriver_A.whenPressed(new DriveSpeedCommand(DriveSubsystem.FULL_SPEED));
            OI.buttonDriver_X.whenPressed(new DriveSpeedCommand(DriveSubsystem.CRUISER_SPEED));
            OI.buttonDriver_START.whenPressed(new DriveSpeedCommand(DriveSubsystem.HALF_SPEED));
            
            OI.buttonDriver_B.whileHeld(new ClawCargoEject());
            OI.buttonDriver_Y.whenPressed(new ClawGamePieceCommand(clawSetPointSubsystem.ClawEjectHatch()));
            
            OI.buttonDriver_LB.whenPressed(new CollectingCargoFromField());
            // OI.buttonDriver_RB.whenPressed(new ArmGamePieceCommand(armSetPointSubsystem.foldedPID()));
            OI.buttonDriver_RB.whenPressed(new Fold());
            OI.buttonDriver_RIGHT_ANALOG.whileHeld(new CollectCommand());

            /////////////////////////////////////// ! OPERATOR joystick !/////////////////////////////////////////////+
            OI.buttonOperator_1.whileHeld(new ClimbManualDown());
            OI.buttonOperator_2.whileHeld(new ClimbManualUP());
            
            OI.buttonOperator_3.whileHeld(new ClawCargoCollect());
            OI.buttonOperator_4.whileHeld(new ArmManualDown());
            
            OI.buttonOperator_7.whenPressed(new CGCargoHigh());
            OI.buttonOperator_9.whenPressed(new CGCargoMid());
            OI.buttonOperator_11.whenPressed(new CGCargoLow());
            OI.buttonOperator_6.whenPressed(new CGCargoShip());
            
            OI.buttonOperator_8.whenPressed(new CGHatchHigh());
            OI.buttonOperator_10.whenPressed(new CGHatchMid());
            OI.buttonOperator_12.whenPressed(new CGHatchLow()); 
            
            OI.buttonOperator_5.whenPressed(new ElevatorGamePieceCommand(elevatorSetPointSubsystem.GRABBING_CARGO_FROM_ROBOT)); 
            
            /////////////////////////////////////// ! Test joystick !/////////////////////////////////////////////////
            OI.buttonTest_1.whileHeld(new FullAutonomousClimb());

            OI.buttonTest_5.whileHeld(new ManualUPElevator()); 
            OI.buttonTest_3.whileHeld(new ManualDownElevator()); 
            OI.buttonTest_2.whenPressed(new ElevatorReset());
            
            // Arm Manual
            OI.buttonTest_6.whileHeld(new ArmManualUp());
            OI.buttonTest_4.whileHeld(new ArmManualDown());
            
            // PivotManual
            OI.buttonTest_7.whileHeld(new PivotManualUP());
            OI.buttonTest_9.whileHeld(new PivotManualDown());
            
            // ClawManual
            OI.buttonTest_8.whileHeld(new ClawOpenManual()); 
            OI.buttonTest_10.whileHeld(new ClawCloseManual());
            OI.buttonTest_11.whileHeld(new EjectCommand());
        } else if (mode == Mode.TEST_1) {
            // OI.buttonOperator_1.whileHeld/*whenPressed*/(new AutonomousClimb()/*new SyncedClimbCG()*/);
            OI.buttonOperator_2.whenPressed(/*new ElevatorReset()*/ new FullAutonomousClimb() );
            
            OI.buttonOperator_5.whileHeld(new ManualUPElevator());
            OI.buttonOperator_3.whileHeld(new ManualDownElevator());
            
            OI.buttonOperator_4.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.GRABBING_CARGO_FROM_ROBOT)); 
            OI.buttonOperator_6.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_COLLECTING_HEIGHT)); 
            
            OI.buttonOperator_11.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_LOW_PID_VALUE));
            OI.buttonOperator_9.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_MID_PID_VALUE));
            OI.buttonOperator_7.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_HIGH_PID_VALUE));
            OI.buttonOperator_12.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.HATCH_LOW_PID_VALUE));
            OI.buttonOperator_10.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.HATCH_MID_PID_VALUE));
            OI.buttonOperator_8.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.HATCH_HIGH_PID_VALUE));
            
            
            OI.buttonDriver_X.whileHeld(new ArmManualUp());
            OI.buttonDriver_A.whileHeld(new ArmManualDown());
            OI.buttonDriver_B.whenPressed(new ArmGamePieceCommand(this.armSetPointSubsystem.cargoPID()));
            OI.buttonDriver_Y.whenPressed(new ArmGamePieceCommand(this.armSetPointSubsystem.foldedPID()));
            OI.buttonDriver_RB.whileHeld(/*new ArmGamePieceCommand(this.armSetPointSubsystem.climbPID())*/new ClawCargoEject());
            OI.buttonDriver_LB.whileHeld(/*new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_SHIP_PID_VALUE)*/new ClawCargoCollect());
            
            OI.buttonDriver_START.whileHeld(new CollectCommand());

            // OI.buttonDriver_START.whenPressed(new PivotGamePieceCommand(PivotSetPointSubsystem.CARGO_ANGLE_PID_OFFSET));
            OI.buttonDriver_BACK.whenPressed(new PivotGamePieceCommand(PivotSetPointSubsystem.HATCH_ANGLE_PID_OFFSET));
            OI.buttonDriver_RIGHT_ANALOG.whileHeld(new PivotManualUP());
            OI.buttonDriver_LEFT_ANALOG.whileHeld(new PivotManualDown());
        } else if (mode == Mode.TEST_2) {
            
            OI.buttonOperator_1.whenPressed(new CollectingCargoFromField());
            OI.buttonOperator_2.whenPressed(new CloseOnCargoCG());
            
            OI.buttonOperator_5.whileHeld(new ClimbManualUP());
            OI.buttonOperator_3.whileHeld(new ClimbManualDown());
            
            OI.buttonOperator_7.whenPressed(new CGCargoHigh());
            OI.buttonOperator_9.whenPressed(new CGCargoMid());
            OI.buttonOperator_11.whenPressed(new CGCargoLow());
            OI.buttonOperator_8.whenPressed(new CGHatchHigh());
            OI.buttonOperator_10.whenPressed(new CGHatchMid());
            OI.buttonOperator_12.whenPressed(new CGHatchLow());
            
            OI.buttonOperator_6.whenPressed(new CGCargoShip());
            OI.buttonOperator_4.whenPressed(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.GRABBING_CARGO_FROM_ROBOT)); 
            
            // OI.buttonDriver_A.whenPressed(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_OPEN_FOR_CARGO));
            // OI.buttonDriver_B.whenPressed(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_CLOSE_ON_CARGO));
            OI.buttonDriver_X.whenPressed(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_EJECT_HATCH));
            OI.buttonDriver_Y.whenPressed(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_HATCH));
            
            OI.buttonDriver_LB.whileHeld(new ClawCloseManual());
            OI.buttonDriver_RB.whileHeld(new ClawOpenManual());
            OI.buttonDriver_RIGHT_ANALOG.whileHeld(new CollectCommand());
            OI.buttonDriver_LEFT_ANALOG.whileHeld(new EjectCommand());
        }
        
        SmartDashboard.putData("SetPIDArm", new SetPIDArmCommand());
        SmartDashboard.putData("SetPIDElevator", new SetPIDElevatorCommand());
        SmartDashboard.putData("SetPIDPivot", new SetPIDPivotCommand());
        SmartDashboard.putData("SetPIDClaw", new SetPIDClawCommand());
        SmartDashboard.putData("SetAutonomousClimbWeights", new SetAutonomousClimbWeights());
        
    }
    
    /**
    * This function is called every robot packet, no matter the mode. Use
    * this for items like diagnostics that you want ran during disabled,
    * autonomous, teleoperated and test.
    *
    * <p>This runs after the mode specific periodic functions, but before
    * LiveWindow and SmartDashboard integrated updating.
    */
    @Override
    public void robotPeriodic() {
        if (m_timer.get() > 1.0) {
            m_timer.reset();
            
            // Put low priority tasks here
        }
        
        if(RobotDrive.getPov() == 0){
            serverForCamera.setSource(DriversCameraFront);
        } else if (RobotDrive.getPov() == 180){
            serverForCamera.setSource(DriversCameraBack);
        }
        elevatorPID.reportVariables();
        armPID.reportVariables();
        pivotPID.reportVariables();
        clawPID.reportVariables();
        climbSubsystem.reportVariabls();
        
        //Dashboard
        SmartDashboard.putNumber("IR", infraredSubsystem.getIRValue());
        SmartDashboard.putBoolean("Cargo In", infraredSubsystem.IsIn());
        SmartDashboard.putBoolean("Elevator switch", elevatorPID.getElevatorSwitch());
        SmartDashboard.putBoolean("Climb Down", climbSubsystem.IsPressedDownSwitch());
        SmartDashboard.putBoolean("Climb UP", climbSubsystem.IsPressedUPSwitch());
        SmartDashboard.putBoolean("Down Left switch", climbSubsystem.downLeftSwitch());
        SmartDashboard.putBoolean("Down Right switch", climbSubsystem.downRighSwitch());
        SmartDashboard.putBoolean("UP Left switch", climbSubsystem.upLeftSwitch());
        SmartDashboard.putBoolean("UP Right switch", climbSubsystem.upRighSwitch());
        
        //debugging
        SmartDashboard.putNumber("POV", RobotDrive.getPov());
    }
    
    /**
    * This function is called once each time the robot enters Disabled mode.
    * You can use it to reset any subsystem information you want to clear when
    * the robot is disabled.
    */
    @Override
    public void disabledInit() {
    }
    
    @Override
    public void disabledPeriodic() {
        Scheduler.getInstance().run();
    }
    
    /**
    * This autonomous (along with the chooser code above) shows how to select
    * between different autonomous modes using the dashboard. The sendable
    * chooser code works with the Java SmartDashboard. If you prefer the
    * LabVIEW Dashboard, remove all of the chooser code and uncomment the
    * getString code to get the auto name from the text box below the Gyro
    *
    * <p>You can add additional auto modes by adding additional commands to the
    * chooser code above (like the commented example) or additional comparisons
    * to the switch structure below with additional strings & commands.
    */
    @Override
    public void autonomousInit() {
        //m_ClawSubsystem = (Subsystem) m_ClawChooser.getSelected();
        //m_ClawSubsystem = m_ClawChooser.getSelected();
        elevatorPID.resetEncoder();
    }
    
    /**
    * This function is called periodically during autonomous.
    */
    @Override
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }
    
    @Override
    public void teleopInit() {
    }
    
    /**
    * This function is called periodically during operator control.
    */
    @Override
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }
    
    /**
    * This function is called periodically during test mode.
    */
    @Override
    public void testPeriodic() {
    }
}
