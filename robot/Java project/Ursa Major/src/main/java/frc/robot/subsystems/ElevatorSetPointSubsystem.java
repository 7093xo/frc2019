/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;

/**
* Add your docs here.
*/
public class ElevatorSetPointSubsystem extends Subsystem {
    public static final double CARGO_HIGH_PID_VALUE = 620000;
    public static final double CARGO_MID_PID_VALUE = 375000;
    public static final double CARGO_LOW_PID_VALUE = 85000;
    public static final double CARGO_SHIP_PID_VALUE = 290000;
    public static final double CARGO_COLLECTING_HEIGHT = 350000;
    
    public static final double HATCH_HIGH_PID_VALUE = 580000;
    public static final double HATCH_MID_PID_VALUE = 304000;
    public static final double HATCH_LOW_PID_VALUE = 30000;
    
    public static final double ZERO_PID_VALUE = 500;
    public static final double GRABBING_CARGO_FROM_ROBOT = 25000;
    
    @Override
    public void initDefaultCommand() {
    }
}
