/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import edu.wpi.first.wpilibj.AnalogPotentiometer; 
import frc.robot.commands.PivotKeepInPlace;

import frc.robot.Robot;
import frc.robot.Robot.PivotState;
/**
* Add your docs here.
*/
public class PivotPIDSubsystem extends PIDSubsystem {
    private WPI_TalonSRX pivotMotor;
    private Potentiometer pivotPotentiometer;
    private double latestPivotPIDOutput = 0;
    //private double initialPotentiometerValue;
    
    private boolean m_isPIDEnabled = true;
    
    public PivotPIDSubsystem() {
        super("PivotPID", 
        RobotMap.Pivot_PID_P,
        RobotMap.Pivot_PID_I,
        RobotMap.Pivot_PID_D);
    	this.pivotPotentiometer = new AnalogPotentiometer(RobotMap.PivotPotentiometer, 270, 0);
        this.pivotMotor = new WPI_TalonSRX(RobotMap.PivotMotor);
        //this.initialPotentiometerValue = this.pivotPotentiometer.get();
        this.pivotMotor.setInverted(true);
        
        setPercentTolerance(7);
        setInputRange(0, 270);
        setOutputRange(-1, 1);
        setPoint(this.pivotPotentiometer.get());
        enable();
    }
    
    @Override
    public void initDefaultCommand() {
        setDefaultCommand(new PivotKeepInPlace());
    }
    
    @Override
    protected double returnPIDInput() {
        return this.pivotPotentiometer.get();
    }
    
    @Override
    protected void usePIDOutput(double output) {
        SmartDashboard.putNumber("Pivot PID output", output);
        this.latestPivotPIDOutput = output;
    }
    
    public double getPivotPIDOutput() {
        return this.latestPivotPIDOutput;
    }
    
    public void enablePID() {
        if (m_isPIDEnabled == false) {
            m_isPIDEnabled = true;
            Robot.m_pivotState = PivotState.PID;
            this.enable();
            this.setDefaultCommand(new PivotKeepInPlace());
        }
    }
    
    public void disablePID() {
        if (m_isPIDEnabled == true) {
            m_isPIDEnabled = false;
            Robot.m_pivotState = PivotState.MANUAL;
            this.disable();
            this.setDefaultCommand(null);
        }
    }
    
    public double getPotentiometerValue() {
        return this.pivotPotentiometer.get();
    }
    
    public void setMotorValue(double value) {
        pivotMotor.set(value);
    }
    
    public void setPoint(double setPointValue) {
        SmartDashboard.putNumber("Pivot SetPoint", setPointValue);

        this.enablePID();
        this.setSetpoint(setPointValue);
    }
    
    public boolean isOnTarget() {
        return this.onTarget();
    }
    
    public void PivotManualUP() {
        this.disablePID();
        pivotMotor.set(1);
    }
    
    public void PivotManualDown() {
        this.disablePID();
        pivotMotor.set(-1);
    }
    
    public void PivotStop(){
        pivotMotor.set(0);
    } 

    public void refreshConfiguration() {
        this.getPIDController().setP(RobotMap.Pivot_PID_P);
        this.getPIDController().setI(RobotMap.Pivot_PID_I);
        this.getPIDController().setD(RobotMap.Pivot_PID_D);
    }

    public void reportVariables() {
        SmartDashboard.putNumber("Pivot Pontetiometer", getPotentiometerValue());
        SmartDashboard.putNumber("Pivot PID P", getPIDController().getP());
        SmartDashboard.putNumber("Pivot PID I", getPIDController().getI());
        SmartDashboard.putNumber("Pivot PID D", getPIDController().getD());
    }
}
