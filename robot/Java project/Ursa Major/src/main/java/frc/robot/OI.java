/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;


/**
* This class is the glue that binds the controls on the physical operator
* interface to the commands and command groups that allow control of the robot.
*/
public class OI {
    //Joysticks
    public static Joystick LeftDriverJoystick = new Joystick(0);
    public static Joystick OperatorJoystick = new Joystick(1);
    public static Joystick TestJoystick = new Joystick(2);
    
    ///////////////////////////////////// ! Test joystick ! //////////////////////////////////////
    public static Button buttonTest_1 = new JoystickButton(TestJoystick, 1);
    public static Button buttonTest_2 = new JoystickButton(TestJoystick, 2);
    public static Button buttonTest_3 = new JoystickButton(TestJoystick, 3);
    public static Button buttonTest_4 = new JoystickButton(TestJoystick, 4);
    public static Button buttonTest_5 = new JoystickButton(TestJoystick, 5);
    public static Button buttonTest_6 = new JoystickButton(TestJoystick, 6);
    public static Button buttonTest_7 = new JoystickButton(TestJoystick, 7);
    public static Button buttonTest_8 = new JoystickButton(TestJoystick, 8);
    public static Button buttonTest_9 = new JoystickButton(TestJoystick, 9);
    public static Button buttonTest_10 = new JoystickButton(TestJoystick, 10);
    public static Button buttonTest_11 = new JoystickButton(TestJoystick, 11);
    public static Button buttonTest_12 = new JoystickButton(TestJoystick, 12);
    
    ///////////////////////////////////// ! DRIVER JOYSTICK ! //////////////////////////////////////
    public static Button buttonDriver_A = new JoystickButton(LeftDriverJoystick, 1);
    public static Button buttonDriver_B = new JoystickButton(LeftDriverJoystick, 2);
    public static Button buttonDriver_X = new JoystickButton(LeftDriverJoystick, 3);
    public static Button buttonDriver_Y = new JoystickButton(LeftDriverJoystick, 4);
    public static Button buttonDriver_LB = new JoystickButton(LeftDriverJoystick, 5); 
    public static Button buttonDriver_RB = new JoystickButton(LeftDriverJoystick, 6);
    public static Button buttonDriver_BACK = new JoystickButton(LeftDriverJoystick, 7);
    public static Button buttonDriver_START  = new JoystickButton(LeftDriverJoystick, 8);
    public static Button buttonDriver_LEFT_ANALOG = new JoystickButton(LeftDriverJoystick, 9);
    public static Button buttonDriver_RIGHT_ANALOG = new JoystickButton(LeftDriverJoystick, 10);
    
    ///////////////////////////////////// ! Operator joystick ! ///////////////////////////////////
    public static Button buttonOperator_1 = new JoystickButton(OperatorJoystick, 1);
    public static Button buttonOperator_2 = new JoystickButton(OperatorJoystick, 2);
    public static Button buttonOperator_3 = new JoystickButton(OperatorJoystick, 3);
    public static Button buttonOperator_4 = new JoystickButton(OperatorJoystick, 4);
    public static Button buttonOperator_5 = new JoystickButton(OperatorJoystick, 5);
    public static Button buttonOperator_6 = new JoystickButton(OperatorJoystick, 6); 
    public static Button buttonOperator_7 = new JoystickButton(OperatorJoystick, 7);
    public static Button buttonOperator_8 = new JoystickButton(OperatorJoystick, 8);
    public static Button buttonOperator_9 = new JoystickButton(OperatorJoystick, 9);
    public static Button buttonOperator_10 = new JoystickButton(OperatorJoystick, 10);
    public static Button buttonOperator_11 = new JoystickButton(OperatorJoystick, 11);
    public static Button buttonOperator_12 = new JoystickButton(OperatorJoystick, 12);

}
