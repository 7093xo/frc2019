/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.Robot;
import frc.robot.subsystems.ClawSetPointSubsystem;
import frc.robot.subsystems.ElevatorSetPointSubsystem; 

//import frc.robot.Robot;
public class CollectingCargoFromField extends CommandGroup {
    /**
    * Add your docs here.
    */
    public CollectingCargoFromField() {
        addParallel(new CollectCommand());
        // addParallel(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_OPEN_FOR_CARGO));
        addParallel(new ArmGamePieceCommand(Robot.armSetPointSubsystem.cargoPID()));
        addParallel(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_COLLECTING_HEIGHT));
        addParallel(new PivotGamePieceCommand(Robot.pivotSetPointSubsystem.Hatch_PID()));
        // addSequential(new WaitingForCargo());
        // addParallel(new StopCollecting());
        // addParallel(new ArmGamePieceCommand(Robot.armSetPointSubsystem.foldedPID()));
    }
}
