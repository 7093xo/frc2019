/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import edu.wpi.first.wpilibj.AnalogPotentiometer; 
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import com.ctre.phoenix.motorcontrol.can.*;
import frc.robot.commands.ClawKeepInPlace;
import frc.robot.Robot.ClawState;

public class ClawPIDSubsystem extends PIDSubsystem {
    
    private WPI_TalonSRX m_clawMotor;
    private Potentiometer m_clawPotentiometer;

    private double m_latestClawPIDOutput;
    private double m_initialPotentiometerValue;
    
    private boolean m_isPIDEnabled = true;

    public ClawPIDSubsystem() {
        super("ClawPIDSubsystem", 
        RobotMap.Claw_PID_P,
        RobotMap.Claw_PID_I,
        RobotMap.Claw_PID_D);
        this.m_clawPotentiometer = new AnalogPotentiometer(RobotMap.ClawPotentiometer, 10, 0);
        this.m_initialPotentiometerValue = this.m_clawPotentiometer.get();
        this.m_clawMotor = new WPI_TalonSRX(RobotMap.ClawMotor);
        this.m_clawMotor.setInverted(true);
        
        setPercentTolerance(1);
        setInputRange(0, 5);
        setOutputRange(-1, 1);
        setPoint(this.m_clawPotentiometer.get());
        enable();
    }
    
    @Override
    public void initDefaultCommand() {
        setDefaultCommand(new ClawKeepInPlace());
    }
    
    @Override
    protected double returnPIDInput() {
        return this.m_clawPotentiometer.get();
    }
    
    @Override
    protected void usePIDOutput(double output) {
        SmartDashboard.putNumber("Claw PID", output);
        this.m_latestClawPIDOutput = output;
    }
    
    public double getClawPIDOutput() {
        return this.m_latestClawPIDOutput;
    }
    
    public void enablePID() {
        if (m_isPIDEnabled == false) {
            m_isPIDEnabled = true;
            Robot.m_clawState = ClawState.PID;
            this.enable();
            this.setDefaultCommand(new ClawKeepInPlace());
        }
    }
    
    public void disablePID() {
        if (m_isPIDEnabled == true) {
            m_isPIDEnabled = false;
            Robot.m_clawState = ClawState.MANUAL;
            this.disable();
            this.setDefaultCommand(null);
        }
    }
    
    public double getInitialPotentiometerValue() {
        return this.m_initialPotentiometerValue;
    }
    
    public double getPotentiometerValue() {
        return this.m_clawPotentiometer.get();
    }  
    
    public void setMotorValue(double value) {
        this.m_clawMotor.set(value);
    }
    
    public void setPoint(double setPointValue) {
        SmartDashboard.putNumber("Claw SetPoint", setPointValue);

        this.enablePID();
        this.setSetpoint(setPointValue);
    }
    
    public boolean isOnTarget() {
        return this.onTarget();
    }
    
    public void manualClose() {
        this.disablePID();
        this.m_clawMotor.set(-1);
    }
    
    public void manualOpen() {
        this.disablePID();
        this.m_clawMotor.set(1);
    }
    
    public void stopMotor() {
        this.m_clawMotor.set(0);
    }

    public void refreshConfiguration() {
        this.getPIDController().setP(RobotMap.Claw_PID_P);
        this.getPIDController().setI(RobotMap.Claw_PID_I);
        this.getPIDController().setD(RobotMap.Claw_PID_D);
    }

    public void reportVariables() {
        SmartDashboard.putNumber("Claw init value", getInitialPotentiometerValue());
        SmartDashboard.putNumber("Claw Pontetiometer", getPotentiometerValue());
        SmartDashboard.putNumber("Claw Diff", getPotentiometerValue() - getInitialPotentiometerValue());
        SmartDashboard.putNumber("Claw PID P", getPIDController().getP());
        SmartDashboard.putNumber("Claw PID I", getPIDController().getI());
        SmartDashboard.putNumber("Claw PID D", getPIDController().getD());
    }
}
