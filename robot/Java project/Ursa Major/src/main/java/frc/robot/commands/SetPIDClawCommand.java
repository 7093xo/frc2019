/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.Robot.PivotState;

public class SetPIDClawCommand extends Command {
  private Boolean terminate = false;

  public SetPIDClawCommand() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    terminate = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    try {

      // Get value from user
      Double P = SmartDashboard.getNumber(RobotMap.Claw_PID_P_Name, Robot.clawPID.getPIDController().getP());
      Double I = SmartDashboard.getNumber(RobotMap.Claw_PID_I_Name, Robot.clawPID.getPIDController().getI());
      Double D = SmartDashboard.getNumber(RobotMap.Claw_PID_D_Name, Robot.clawPID.getPIDController().getD());

      // Save
      Preferences prefs = Preferences.getInstance();
      prefs.putDouble(RobotMap.Claw_PID_P_Name, P);
      prefs.putDouble(RobotMap.Claw_PID_I_Name, I);
      prefs.putDouble(RobotMap.Claw_PID_D_Name, D);
      
      // Refresh the parameter tree
      RobotMap.refreshConfiguration();
  } catch (Exception e) {
      System.err.println("Error configuring Claw PID");
  }

  terminate = true;
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return terminate;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
