/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.subsystems.ElevatorSetPointSubsystem;
import frc.robot.subsystems.ClawSetPointSubsystem;
import frc.robot.subsystems.ClawCargoSubsystem;

public class CloseOnCargoCG extends CommandGroup {
    /**
    * Add your docs here.
    */
    public CloseOnCargoCG() {
        addSequential(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.GRABBING_CARGO_FROM_ROBOT));
        // addParallel(new ClawGamePieceCommand(ClawSetPointSubsystem.POSITION_CLOSE_ON_CARGO));
        // addSequential(new ClawCargoCollect());
    }
}
