/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;

/**
* Add your docs here.
*/
public class ClawSetPointSubsystem extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public static final double POSITION_HATCH = 2.75;
    public static final double POSITION_EJECT_HATCH = 0.7;
    
    // private double m_initialPotentiometerValue;
    
    public ClawSetPointSubsystem(double initialPotentiometerValue) {
        // this.m_initialPotentiometerValue = initialPotentiometerValue;
    }   
    
    public double ClawHatch() {
        return POSITION_HATCH;
    }
    
    public double ClawEjectHatch() {
        return POSITION_EJECT_HATCH;
    }
    
    @Override
    public void initDefaultCommand() {
    }
}
