/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.OI;
import frc.robot.RobotMap;
import frc.robot.commands.DriveWithJoysticks;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

/**
* Add your docs here.
*/
public class DriveSubsystem extends Subsystem {

    public final static double FULL_SPEED = 1;
    public final static double CRUISER_SPEED = 0.8;
    public final static double HALF_SPEED = 0.65;
    public final static double DEFAULT_SPEED = 0.8;
    private double m_speed = DEFAULT_SPEED;

    //Left Drive Talons
    public WPI_TalonSRX LeftBackDriveMotor = new WPI_TalonSRX(RobotMap.LeftBackDriveMotor);
    public WPI_TalonSRX LeftMiddleDriveMotor = new WPI_TalonSRX(RobotMap.LeftMiddleDriveMotor);
    public WPI_TalonSRX LeftFrontDriveMotor = new WPI_TalonSRX(RobotMap.LeftFrontDriveMotor);
    
    //Right Drive Talons
    public WPI_TalonSRX RightBackDriveMotor = new WPI_TalonSRX(RobotMap.RightBackDriveMotor);
    public WPI_TalonSRX RightMiddleDriveMotor = new WPI_TalonSRX(RobotMap.RightMiddleDriveMotor);
    public WPI_TalonSRX RightFrontDriveMotor = new WPI_TalonSRX(RobotMap.RightFrontDriveMotor);
    
    //United States of Motors
    public SpeedControllerGroup LeftDrive = new SpeedControllerGroup(LeftBackDriveMotor, LeftMiddleDriveMotor, LeftFrontDriveMotor);
    public SpeedControllerGroup RightDrive = new SpeedControllerGroup(RightBackDriveMotor, RightMiddleDriveMotor, RightFrontDriveMotor);
    
    //DifferentialDrive
    public DifferentialDrive m_Drive = new DifferentialDrive(LeftDrive, RightDrive);
    
    //Slider
    /*double Speed = (OI.LeftDriverJoystick.getRawAxis(3));
    */
    public DriveSubsystem() {
        EnableCurrentLimitation();
    }

    public void EnableCurrentLimitation() { 
        int peakAmps = 25;
        int peakDuration = 0;
        int continuousCurrentLimit = 25;

        LeftBackDriveMotor.enableCurrentLimit(true);
        LeftBackDriveMotor.configPeakCurrentLimit(peakAmps);
        LeftBackDriveMotor.configPeakCurrentDuration(peakDuration);
        LeftBackDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);

        LeftMiddleDriveMotor.enableCurrentLimit(true);
        LeftMiddleDriveMotor.configPeakCurrentLimit(peakAmps);
        LeftMiddleDriveMotor.configPeakCurrentDuration(peakDuration);
        LeftMiddleDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);
        
        LeftFrontDriveMotor.enableCurrentLimit(true);
        LeftFrontDriveMotor.configPeakCurrentLimit(peakAmps);
        LeftFrontDriveMotor.configPeakCurrentDuration(peakDuration);
        LeftFrontDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);

        RightBackDriveMotor.enableCurrentLimit(true);
        RightBackDriveMotor.configPeakCurrentLimit(peakAmps);
        RightBackDriveMotor.configPeakCurrentDuration(peakDuration);
        RightBackDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);

        RightMiddleDriveMotor.enableCurrentLimit(true);
        RightMiddleDriveMotor.configPeakCurrentLimit(peakAmps);
        RightMiddleDriveMotor.configPeakCurrentDuration(peakDuration);
        RightMiddleDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);

        RightFrontDriveMotor.enableCurrentLimit(true);
        RightFrontDriveMotor.configPeakCurrentLimit(peakAmps);
        RightFrontDriveMotor.configPeakCurrentDuration(peakDuration);
        RightFrontDriveMotor.configContinuousCurrentLimit(continuousCurrentLimit);
    }

    public void setSpeed(double speed) {
        this.m_speed = speed; 
    }
    
    //Method for Driving robot
    public void drive() {
        //calculation speed
        //double Speed = (OI.LeftDriverJoystick.getRawAxis(3) / -2 + 0.5);
        /*double AxisY = OI.LeftDriverJoystick.getRawAxis(1);
        double AxisX = OI.LeftDriverJoystick.getRawAxis(4);
        */
        double RT = OI.LeftDriverJoystick.getRawAxis(3);
        double LT = OI.LeftDriverJoystick.getRawAxis(2);
        double drive = (LT * -1 + RT) * m_speed;
        double rotation = OI.LeftDriverJoystick.getRawAxis(0) * m_speed;
        m_Drive.arcadeDrive(drive, -rotation);
    }

    public void setDrive(double speed) {
        m_Drive.arcadeDrive(speed, 0);
    }

    public int getPov(){
        return OI.LeftDriverJoystick.getPOV();
    } 
    
    @Override
    public void initDefaultCommand() {
        setDefaultCommand(new DriveWithJoysticks());
    }
}
