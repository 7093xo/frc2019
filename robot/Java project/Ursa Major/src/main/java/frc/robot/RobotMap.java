/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Preferences;

/**
* The RobotMap is a mapping from the ports sensors and actuators are wired into
* to a variable name. This provides flexibility changing wiring, makes checking
* the wiring easier and significantly reduces the number of magic numbers
* floating around.
*/
public class RobotMap {
    
    //Left Drive Talons
    public static int LeftBackDriveMotor = 0;
    public static int LeftMiddleDriveMotor = 1;
    public static int LeftFrontDriveMotor = 2;
    
    //Right Drive Talons
    public static int RightBackDriveMotor = 3;
    public static int RightMiddleDriveMotor = 4;
    public static int RightFrontDriveMotor = 5;
    
    //Collector Motors
    public static int CollectorMotor = 6;
    
    //Arm Motor
    public static int LeftArmMotor = 7;
    public static int RightArmMotor = 8;
    
    //Elevator Motor
    public static int ElevatorMotor = 9;
    
    //Climbing Motors 
    public static int LeftClimbMotor = 10;
    public static int RightClimbMotor = 11;
    
    //Pivot Motor
    public static int PivotMotor = 12;
    
    //Claw Motor
    public static int ClawMotor = 13; 
    public static int ClawCargoMotor = 14;
    
    //Potentiometer
    public static int ArmPotentiometer = 0;
    public static int PivotPotentiometer = 1; //1
    //public static int ClimbPotentiometer = 2; //2 
    public static int ClawPotentiometer = 3; //3
    
    //IR Sensor
    public static int IRSensor = 2;
    
    // Climb Switches
    public static int downLeftClimbSwitch = 3;
    public static int downRightClimbSwitch = 4;
    public static int upLeftClimbSwitch = 6;
    public static int upRightClimbSwitch = 5;

    
    //hall effect
    // DigitalInput PivotHall = new DigitalInput(3);

    public static Double Arm_PID_P = 0.0038;
    public static String Arm_PID_P_Name = "Arm_PID_P";
    public static Double Arm_PID_I = 0.0;
    public static String Arm_PID_I_Name = "Arm_PID_I";
    public static Double Arm_PID_D = 0.0015;
    public static String Arm_PID_D_Name = "Arm_PID_D";

    public static Double Elevator_PID_P = 0.000001;
    public static String Elevator_PID_P_Name = "Elevator_PID_P";
    public static Double Elevator_PID_I = 0.00000001;
    public static String Elevator_PID_I_Name = "Elevator_PID_I";
    public static Double Elevator_PID_D = 0.0000035;
    public static String Elevator_PID_D_Name = "Elevator_PID_D";

    public static Double Pivot_PID_P = 0.07;
    public static String Pivot_PID_P_Name = "Pivot_PID_P";
    public static Double Pivot_PID_I = 0.0;
    public static String Pivot_PID_I_Name = "Pivot_PID_I";
    public static Double Pivot_PID_D = 0.0;
    public static String Pivot_PID_D_Name = "Pivot_PID_D";

    public static Double Claw_PID_P = 2.3; //2
    public static String Claw_PID_P_Name = "Claw_PID_P";
    public static Double Claw_PID_I = 0.01;
    public static String Claw_PID_I_Name = "Claw_PID_I";
    public static Double Claw_PID_D = 0.08;
    public static String Claw_PID_D_Name = "Claw_PID_D";

    public static Double Roll_P = 1.0;
    public static String Roll_P_Name = "Roll_P";
    public static Double Pitch_P = 1.0;
    public static String Pitch_P_Name = "Pitch_P";

    public static void refreshConfiguration() {
        Preferences prefs = Preferences.getInstance();
        Arm_PID_P = prefs.getDouble(Arm_PID_P_Name, Arm_PID_P);
        Arm_PID_I = prefs.getDouble(Arm_PID_I_Name, Arm_PID_I);
        Arm_PID_D = prefs.getDouble(Arm_PID_D_Name, Arm_PID_D);

        Elevator_PID_P = prefs.getDouble(Elevator_PID_P_Name, Elevator_PID_P);
        Elevator_PID_I = prefs.getDouble(Elevator_PID_I_Name, Elevator_PID_I);
        Elevator_PID_D = prefs.getDouble(Elevator_PID_D_Name, Elevator_PID_D);

        Pivot_PID_P = prefs.getDouble(Pivot_PID_P_Name, Pivot_PID_P);
        Pivot_PID_I = prefs.getDouble(Pivot_PID_I_Name, Pivot_PID_I);
        Pivot_PID_D = prefs.getDouble(Pivot_PID_D_Name, Pivot_PID_D);

        Claw_PID_P = prefs.getDouble(Claw_PID_P_Name, Claw_PID_P);
        Claw_PID_I = prefs.getDouble(Claw_PID_I_Name, Claw_PID_I);
        Claw_PID_D = prefs.getDouble(Claw_PID_D_Name, Claw_PID_D);

        Roll_P = prefs.getDouble(Roll_P_Name, Roll_P);
        Pitch_P = prefs.getDouble(Pitch_P_Name, Pitch_P);

        Robot.armPID.refreshConfiguration();
        Robot.elevatorPID.refreshConfiguration();
        Robot.pivotPID.refreshConfiguration();
        Robot.clawPID.refreshConfiguration();
    }

}
