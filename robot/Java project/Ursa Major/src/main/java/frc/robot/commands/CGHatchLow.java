/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.Robot;
import frc.robot.subsystems.ElevatorSetPointSubsystem;

public class CGHatchLow extends CommandGroup {
    /**
    * Add your docs here.
    */
    public CGHatchLow() {
        addParallel(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.HATCH_LOW_PID_VALUE));
        addParallel(new PivotGamePieceCommand(Robot.pivotSetPointSubsystem.Hatch_PID()));
        addParallel(new ClawGamePieceCommand(Robot.clawSetPointSubsystem.ClawHatch()));
    }
}
