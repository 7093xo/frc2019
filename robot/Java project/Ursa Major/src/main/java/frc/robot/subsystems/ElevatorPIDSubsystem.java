/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.ElevatorKeepInPlace;
import frc.robot.Robot.ElevatorState;

/**
* Add your docs here.
*/
public class ElevatorPIDSubsystem extends PIDSubsystem {
    
    private WPI_TalonSRX m_motor;
    private Encoder m_encoder;
    private double m_lastOutput;
    private DigitalInput m_switch;

    private boolean m_isPIDEnabled = true;
    private static Double Elevator_PID_P = 0.00001;
    private static Double Elevator_PID_I = 0.00000005;
    private static Double Elevator_PID_D = 0.00002;
    

    public ElevatorPIDSubsystem() {

        super("ElevatorPID", 
            ElevatorPIDSubsystem.Elevator_PID_P,
            ElevatorPIDSubsystem.Elevator_PID_I,
            ElevatorPIDSubsystem.Elevator_PID_D);
        // super("ElevatorPID", 0.00003, 0.00000001, 0.00005);
        this.m_switch = new DigitalInput(2);
        this.m_motor = new WPI_TalonSRX(RobotMap.ElevatorMotor);
        this.m_encoder = new Encoder(0, 1, false, EncodingType.k2X);
        resetEncoder();
        this.m_motor.setInverted(true);
        
        setPercentTolerance(8);
        setInputRange(0, 650000);
        setOutputRange(-0.55, 1);
        enable();
        EnableCurrentLimitation();


    }
    public void EnableCurrentLimitation() { 
        int peakAmps = 30;
        int peakDuration = 150;
        int continuousCurrentLimit = 30;

        // m_motor.enableCurrentLimit(true);
        m_motor.configPeakCurrentLimit(peakAmps);
        m_motor.configPeakCurrentDuration(peakDuration);
        m_motor.configContinuousCurrentLimit(continuousCurrentLimit);
    }
    
    @Override
    public void initDefaultCommand() {
       this.setDefaultCommand(new ElevatorKeepInPlace());
    }

 

    public void enablePID() {
        if (m_isPIDEnabled == false) {
            m_isPIDEnabled = true;
            Robot.m_elevatorState = ElevatorState.PID;
            this.enable();
            this.setDefaultCommand(new ElevatorKeepInPlace());
        }
    }

    public void disablePID() {
        if (m_isPIDEnabled == true) {
            m_isPIDEnabled = false;
            Robot.m_elevatorState = ElevatorState.MANUAL;
            this.disable();
            this.setDefaultCommand(null);
        }
    }
    
    @Override
    protected double returnPIDInput() {
        return this.m_encoder.getRaw();
    }
    
    @Override
    protected void usePIDOutput(double output) {
        SmartDashboard.putNumber("Elevator PID", output);
        this.m_lastOutput = output;
    }

    public double getElevatorPIDOutput() {
        return this.m_lastOutput;
    }

    public boolean isOnTarget() {
        return this.onTarget(); 
    }
    
    public void setMotorValue(double value) {
        this.m_motor.set(value);
    }
    
    public void setPoint(double setPointValue) {
        SmartDashboard.putNumber("Elevator SetPoint", setPointValue);
        
        this.enablePID();
        this.setSetpoint(setPointValue);
    }
    
    public void resetEncoder() {
        this.m_encoder.reset();
    }
    
    public boolean getElevatorSwitch() {
        return this.m_switch.get();
    }
    
    public double getEncoderValue() {
        return this.m_encoder.getRaw();
    }
    
    public void manualUp() {
        this.disablePID();
        this.m_motor.set(.7);
    }
    
    public void manualDown() {
        this.disablePID();
        this.m_motor.set(-.35);
    }
    
    public void stopMotor() {
        this.m_motor.stopMotor();
    }
    
    public void resetMotor() {
        this.disablePID();
        this.m_motor.set(-.2);
    }

    public void refreshConfiguration() {
        // this.getPIDController().setP(RobotMap.Elevator_PID_P);
        // this.getPIDController().setI(RobotMap.Elevator_PID_I);
        // this.getPIDController().setD(RobotMap.Elevator_PID_D);
    }

    public void reportVariables() {
        SmartDashboard.putBoolean("ElevatorSwitch", getElevatorSwitch());
        SmartDashboard.putNumber("Elevator Encoder", getEncoderValue());
        SmartDashboard.putNumber("Elevator PID P", getPIDController().getP());
        SmartDashboard.putNumber("Elevator PID I", getPIDController().getI());
        SmartDashboard.putNumber("Elevator PID D", getPIDController().getD());
    }
    
}
