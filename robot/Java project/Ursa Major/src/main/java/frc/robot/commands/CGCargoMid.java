/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.subsystems.ElevatorSetPointSubsystem;
import frc.robot.subsystems.PivotSetPointSubsystem;


public class CGCargoMid extends CommandGroup {
    /**
    * Add your docs here.
    */
    public CGCargoMid() {
        addParallel(new ElevatorGamePieceCommand(ElevatorSetPointSubsystem.CARGO_MID_PID_VALUE));
        addParallel(new PivotGamePieceCommand(PivotSetPointSubsystem.CARGO_ANGLE_PID_OFFSET));
    }
}
