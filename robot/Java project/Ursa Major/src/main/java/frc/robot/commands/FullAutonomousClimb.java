/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.Robot;

public class FullAutonomousClimb extends CommandGroup {
    /**
    * Add your docs here.
    */
    public FullAutonomousClimb() {
        // collect
        addParallel(new CollectCommand());
        
        // climb
        addSequential(new AutonomousClimb());

        addParallel(new armKeep());
        
        // keep in place arm
        // addParallel(new ArmGamePieceCommand(Robot.armSetPointSubsystem.climbPID()));


        // addSequential(new GoForward(3));
        // addSequential(new LiftLegs());
        // addSequential(new GoForward(1.5));

        // cancel keep in place commands
        // if there's time: 
        // move forward (dependent on time)
        // lift legs
        // move forward
    }
}
