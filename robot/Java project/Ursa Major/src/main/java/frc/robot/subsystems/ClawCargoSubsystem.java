/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.*;

/**
* Add your docs here.
*/
public class ClawCargoSubsystem extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private WPI_TalonSRX ClawCargoMotor; 
    
    public ClawCargoSubsystem() {
        ClawCargoMotor = new WPI_TalonSRX(RobotMap.ClawCargoMotor);
        // ClawCargoMotor.setNeutralMode(NeutralMode.Brake);
    } 

    //Collect
    public void ClawCargoCollect(){
        this.ClawCargoMotor.set(-0.7);
    }
    //Eject
    public void ClawCargoEject(){
        this.ClawCargoMotor.set(1);
    }
    //Stop Motor
    public void ClawCargoStop(){
        this.ClawCargoMotor.stopMotor();
    }
    
    @Override
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        // setDefaultCommand(new MySpecialCommand());
    }
}
