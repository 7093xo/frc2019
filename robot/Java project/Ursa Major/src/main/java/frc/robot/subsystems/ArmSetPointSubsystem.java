/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
* Add your docs here.
*/
public class ArmSetPointSubsystem extends Subsystem {
    
    public static final double CARGO_PID_VALUE_OFFSET = 135 * 1.86;
    public static final double CLIMB_PID_VALUE_OFFSET =  485; // TODO: find out
    public static final double FOLDED_PID_VALUE_OFFSET = 5 * 1.86;
    
    private double m_initialPotentiometerValue;
    
    public ArmSetPointSubsystem(double initialPotentiometerValue) {
        this.m_initialPotentiometerValue = initialPotentiometerValue;
    }
    
    public double cargoPID() {
        return this.m_initialPotentiometerValue + CARGO_PID_VALUE_OFFSET;
    }
    
    public double foldedPID() {
        return this.m_initialPotentiometerValue + FOLDED_PID_VALUE_OFFSET;
    }
    
    public double climbPID() {
        return this.m_initialPotentiometerValue + CLIMB_PID_VALUE_OFFSET;
    }
    
    @Override
    public void initDefaultCommand() {
    }
    
}
