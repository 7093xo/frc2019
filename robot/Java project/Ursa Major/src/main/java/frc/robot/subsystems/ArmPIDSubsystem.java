/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.*;
import frc.robot.Robot;
import frc.robot.Robot.ArmState;
import frc.robot.RobotMap;
import frc.robot.commands.ArmKeepInPlace;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

public class ArmPIDSubsystem extends PIDSubsystem {
    
    private Potentiometer m_armPotentiometer;
    private double m_latestARMPIDOutput;
    private WPI_TalonSRX m_rightMotor;
    private WPI_TalonSRX m_leftMotor;
    private SpeedControllerGroup m_armMotor;
    private double m_initialPotentiometerValue;
    private static final double MANUAL_DOWN_MOTOR_VALUE = -0.8; // TODO: use these later instead of constants
    private static final double MANUAL_UP_MOTOR_VALUE = 0.8;

    private boolean m_isPIDEnabled = true;
    
    public ArmPIDSubsystem() {
        super(
            "ArmPID",
            RobotMap.Arm_PID_P,
            RobotMap.Arm_PID_I,
            RobotMap.Arm_PID_D);
    
        this.m_armPotentiometer = new AnalogPotentiometer(RobotMap.ArmPotentiometer, 3600, 0);
        this.m_initialPotentiometerValue = this.m_armPotentiometer.get();
        this.m_rightMotor = new WPI_TalonSRX(RobotMap.RightArmMotor);
        this.m_leftMotor = new WPI_TalonSRX(RobotMap.LeftArmMotor);
        this.m_leftMotor.setInverted(true);
        this.m_rightMotor.setInverted(true);
        this.m_armMotor = new SpeedControllerGroup(m_leftMotor, m_rightMotor);
    
        setPercentTolerance(5);
        setInputRange(m_initialPotentiometerValue, m_initialPotentiometerValue + 300);
        setOutputRange(-1, 1);
        enable();
    }
    
    @Override
    public void initDefaultCommand() {
       setDefaultCommand(new ArmKeepInPlace());
    }
    
    @Override
    protected double returnPIDInput() {
        return this.m_armPotentiometer.get();
    }

    @Override
    protected void usePIDOutput(double output) {
        SmartDashboard.putNumber("Arm PID", output);
        this.m_latestARMPIDOutput = output;
    }
    
    //Create method for getting PID Output
    public double getArmPIDOutput() {
        return this.m_latestARMPIDOutput;
    }

    public void enablePID() {
        if (m_isPIDEnabled == false) {
            m_isPIDEnabled = true;
            Robot.m_armState = ArmState.PID;
            this.enable();
            this.setDefaultCommand(new ArmKeepInPlace());
        }
    }

    public void disablePID() {
        if (m_isPIDEnabled == true) {
            m_isPIDEnabled = false;
            Robot.m_armState = ArmState.MANUAL;
            this.disable();
            this.setDefaultCommand(null);
        }
    }

    public double getInitialPotentiometerValue() {
        return this.m_initialPotentiometerValue;
    }

    public double getPotentiometerValue() {
        return this.m_armPotentiometer.get();
    }  

    public void setMotorValue(double value) {
        this.m_armMotor.set(-value);
        
    }

    public void setPoint(double setPointValue) {
        SmartDashboard.putNumber("Arm SetPoint", setPointValue);

        this.enablePID();
        this.setSetpoint(setPointValue);
    }

    public boolean isOnTarget() {
        return this.onTarget();
    }

    public void manualUp() {
        this.disablePID();
        this.m_armMotor.set(0.8);
    }
      
    public void manualDown() {
        this.disablePID();
        this.m_armMotor.set(-0.8);
    }

    public void climbControl(double value) {
        this.disablePID();
        this.m_armMotor.set(value);
    }

    public void stopMotor() {
        this.m_armMotor.set(0);
    }

    public void refreshConfiguration() {
        this.getPIDController().setP(RobotMap.Arm_PID_P);
        this.getPIDController().setI(RobotMap.Arm_PID_I);
        this.getPIDController().setD(RobotMap.Arm_PID_D);
    }

    public void reportVariables() {
        SmartDashboard.putNumber("Arm init value", getInitialPotentiometerValue());
        SmartDashboard.putNumber("Arm Pontetiometer", getPotentiometerValue());
        SmartDashboard.putNumber("Arm Diff", getPotentiometerValue() - getInitialPotentiometerValue());
        SmartDashboard.putNumber("Arm PID P", getPIDController().getP());
        SmartDashboard.putNumber("Arm PID I", getPIDController().getI());
        SmartDashboard.putNumber("Arm PID D", getPIDController().getD());
    }
}
