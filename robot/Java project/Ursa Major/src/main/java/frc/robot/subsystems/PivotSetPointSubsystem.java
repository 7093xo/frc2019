/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;


/**
* Add your docs here.
*/
public class PivotSetPointSubsystem extends Subsystem {
    
    public static final double CARGO_ANGLE_PID_OFFSET = 165;
    public static final double HATCH_ANGLE_PID_OFFSET = 72;
    
    public PivotSetPointSubsystem(){
    }
    
    private double getValue(double val) { // TODO: delete later
        return val;
    }
    
    public double Hatch_PID(){
        return HATCH_ANGLE_PID_OFFSET;
    }
    
    public double Cargo_PID(){
        return CARGO_ANGLE_PID_OFFSET;
    }

    public double cargoHigh_PID(){
        return CARGO_ANGLE_PID_OFFSET + 20;
    }
    
    @Override
    public void initDefaultCommand() {
    }
}
