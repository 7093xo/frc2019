/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.*;
import frc.robot.RobotMap;

/**
* Add your docs here.
*/
public class CollectorSubsystem extends Subsystem {

    public WPI_TalonSRX CollectorMotor = new WPI_TalonSRX(RobotMap.CollectorMotor);
    
    public void Collect(){
        this.CollectorMotor.set(.6);
    }
    
    public void Eject(){
        this.CollectorMotor.set(-.5);
    }
    
    public void Stop(){
        this.CollectorMotor.set(0);
    }
    
    @Override
    public void initDefaultCommand() {
        Stop(); // TODO: why is this here?
    }
}
